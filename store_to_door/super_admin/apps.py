from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'super_admin'
