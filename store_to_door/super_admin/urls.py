from django.conf.urls import url

from super_admin import views

urlpatterns = [
    url(r'^create/product-category/$', views.ProductCategoryView.as_view(), name='create_product_category'),
    url(r'^create/product-sub-category/$', views.ProductSubCategoryView.as_view(), name='create_product_sub_category'),
    url(r'^create/product/$', views.ProductView.as_view(), name='create_product'),
    url(r'^create/store/$', views.StoreView.as_view(), name='create_store'),
    url(r'^create/store-category/$', views.StoreCategoryView.as_view(), name='create_store_category'),
    url(r'^create/store-sub-category/$', views.StoreSubCategoryView.as_view(), name='create_store_sub_category'),
    url(r'^create/store-product/$', views.StoreProductView.as_view(), name='create_store_product'),
    url(r'^create/daily-offer/$', views.DailyOfferView.as_view(), name='create_daily_offer'),
    url(r'^create/promotion/$', views.PromotionView.as_view(), name='create_promotion'),
]
