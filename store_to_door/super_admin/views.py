from rest_framework import generics

from super_admin import serializers


class ProductCategoryView(generics.CreateAPIView):
    """
    use this endpoint to create a product category.
    """
    serializer_class = serializers.ProductCategorySerializer


class ProductSubCategoryView(generics.CreateAPIView):
    """
    use this endpoint to create a product sub-category.
    """
    serializer_class = serializers.ProductSubCategorySerializer


class ProductView(generics.CreateAPIView):
    """
    use this endpoint to create a product.
    """
    serializer_class = serializers.ProductSerializer


class StoreView(generics.CreateAPIView):
    """
    use this endpoint to create a store.
    """
    serializer_class = serializers.StoreSerializer


class StoreCategoryView(generics.CreateAPIView):
    """
    use this endpoint to create a store-category.
    """
    serializer_class = serializers.StoreCategorySerializer


class StoreSubCategoryView(generics.CreateAPIView):
    """
    use this endpoint t create a store-sub_category.
    """
    serializer_class = serializers.StoreSubCategorySerializer


class StoreProductView(generics.CreateAPIView):
    """
    use this endpoint to create a store-product.
    """
    serializer_class = serializers.StoreProductSerializer


class DailyOfferView(generics.CreateAPIView):
    """
    use this endpoint to create daily offers.
    """
    serializer_class = serializers.DailyOfferSerializer


class PromotionView(generics.CreateAPIView):
    """
    use this endpoint to create a promotion.
    """
    serializer_class = serializers.PromotionSerializer
