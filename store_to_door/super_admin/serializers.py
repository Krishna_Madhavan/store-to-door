from rest_framework import serializers

from users import models


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductCategory
        fields = ("id", "name", "image", "description")


class ProductSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductSubCategory
        fields = ("id", "category", "name", "image")


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = ("id", "sub_category", "name", "unit", "apparent_price", "actual_price", "discount",
                  "description", "contents", "image")


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Store
        fields = ("id", "name", "delivery_time", "free_deliver_cost", "rating")


class StoreCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StoreCategory
        fields = ("id", "store", "category")


class StoreSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StoreSubCategory
        fields = ("id", "store_category", "sub_category")


class StoreProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StoreProduct
        fields = ("id", "store_subcategory", "product")


class DailyOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DailyOffer
        fields = ("id", "product", "discount")


class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Promotion
        fields = ("id", "sub_category", "image")
