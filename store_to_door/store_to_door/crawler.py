import random
import time

from django.core.exceptions import ObjectDoesNotExist
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains

from users import models

driver = webdriver.Chrome('C:/Users/Krishna/Documents/chromedriver')
driver.get("https://grofers.com/")
time.sleep(5)

location = driver.find_element_by_class_name("location__selected")
location.click()
time.sleep(5)

categories = driver.find_elements_by_class_name("category-name__name")[12:13]
for index, category in enumerate(categories):
    category_object = models.ProductCategory.objects.get(pk=13)
    ActionChains(driver).move_to_element(category).perform()
    category.click()
    time.sleep(5)

    sub_categories = driver.find_elements_by_class_name("category-list__item")
    for sub_category in sub_categories[1:]:
        ActionChains(driver).move_to_element(sub_category).perform()
        sub_category.click()
        time.sleep(5)

        sub_category_name = sub_category.text.strip()
        sub_category_object, created = models.ProductSubCategory.objects.get_or_create(category=category_object,
                                                                                       name=sub_category_name)

        products = driver.find_elements_by_class_name("product__wrapper")
        for product in products:
            ActionChains(driver).move_to_element(product).perform()
            product.click()
            time.sleep(2)

            try:
                product_name = driver.find_element_by_class_name("pdp-product__name").text.strip()
            except NoSuchElementException:
                modal_close_btn = driver.find_element_by_class_name("modal-close__btn")
                modal_close_btn.click()
                continue
            try:
                models.Product.objects.get(sub_category=sub_category_object, name=product_name)
            except ObjectDoesNotExist:
                pass
            else:
                modal_close_btn = driver.find_element_by_class_name("modal-close__btn")
                modal_close_btn.click()
                continue
            try:
                product_unit = driver.find_element_by_class_name("pdp-product__selected-variant").text.strip()
            except NoSuchElementException:
                product_unit = "1 Kg"
            try:
                actual_price = driver.find_element_by_class_name("pdp-product__price--new").text.strip()[1:]
            except NoSuchElementException:
                modal_close_btn = driver.find_element_by_class_name("modal-close__btn")
                modal_close_btn.click()
                continue
            try:
                apparent_price = driver.find_element_by_class_name("pdp-product__price--old").text.strip()[1:]
            except NoSuchElementException:
                apparent_price = int(actual_price) + random.randint(10, 30)
            try:
                discount = driver.find_element_by_class_name("offer-text").text.strip()
            except NoSuchElementException:
                discount = 0.0
            try:
                image = driver.find_element_by_class_name("pdp-slider__images").get_attribute("src")
            except NoSuchElementException:
                image = None

            models.Product.objects.create(sub_category=sub_category_object, name=product_name, unit=product_unit,
                                          apparent_price=apparent_price, actual_price=actual_price,
                                          discount=discount, image=image)

            modal_close_btn = driver.find_element_by_class_name("modal-close__btn")
            modal_close_btn.click()

driver.quit()
