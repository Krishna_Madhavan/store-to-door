from django.utils.translation import ugettext_lazy as _

EMAIL_ALREADY_EXISTS = _('An account with this email address already exists.')
NUMBER_ALREADY_EXISTS = _("An account with this mobile number already exists.")
INVALID_ROLE = _("This role is invalid.")
UNAUTHORISED_USER = _("An user with this id does not exists.")
ACCESS_TOKEN_ERROR = _("Error verifying the access token.")
INVALID_PRODUCT = _("This product is invalid.")
INVALID_ADDRESS = _("This address is invalid.")
EMPTY_CART = _("The cart is empty.")
INVALID_STORE = _("This store is invalid.")
ITEM_ALREADY_IN_CART = _("This item is already in your cart.")
INVALID_QUANTITY_ERROR = _("Make sure the quantity is less than 9 and greater than 0.")

# Facebook Account Kit Keys
facebook_app_id = 2039396169616790
app_secret = "2de8bc59fbd212790ef2d87b4c856952"

# Razorpay Keys
YOUR_API_KEY = "rzp_test_twhGB9MZHnXYT0"
YOUR_API_SECRET = "RlLqYLmmnBYkhz3Z9OTe0Spg"
