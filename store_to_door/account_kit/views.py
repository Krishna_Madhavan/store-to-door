import requests
import django.contrib.auth.signals
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework import generics, exceptions, response, status, permissions

from account_kit import serializers, utils
from store_to_door import constants


class RegistrationView(generics.CreateAPIView):
    """
    use this endpoint to register an user.
    """
    serializer_class = serializers.RegistrationSerializer


class ActivationView(utils.ActionViewMixin, utils.GenerateTokenMixin, generics.GenericAPIView):
    """
    use this endpoint to activate an account by authenticating the user using facebook account kit.
    """
    serializer_class = serializers.ActivationSerializer

    def __init__(self, **kwargs):
        super(ActivationView, self).__init__(**kwargs)
        self.authorization_code = None
        self.user = None

    @transaction.atomic()
    def action(self, serializer):
        self.authorization_code = serializer.data["authorization_code"]
        self.user = self.int_to_user(serializer.data["username"])
        url = "https://graph.accountkit.com/v1.2/access_token?grant_type=authorization_code&" \
              "code={}&access_token=AA|{}|{}".format(self.authorization_code, constants.facebook_app_id,
                                                     constants.app_secret)
        api_response = requests.get(url=url)
        if api_response.status_code == 400:
            raise exceptions.ValidationError({"non_field_errors": [constants.ACCESS_TOKEN_ERROR]})
        json_response = api_response.json()
        if "access_token" not in json_response and not json_response["access_token"]:
            raise exceptions.ValidationError({"non_field_errors": [constants.ACCESS_TOKEN_ERROR]})
        self.user.token = self.get_or_create_token(serializer_data=serializer.data)
        self.user.is_active = True
        self.user.save()
        data = serializers.RegistrationSerializer(self.user, context={'request': self.request}).data
        return response.Response(data=data)

    @staticmethod
    def int_to_user(username):
        try:
            return User.objects.get(username=username)
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.UNAUTHORISED_USER]})


class ValidateUserView(utils.ActionViewMixin, utils.GenerateTokenMixin, generics.GenericAPIView):
    """
    use this endpoint to validate a login request.
    """
    serializer_class = serializers.ValidateUserSerializer

    def __init__(self, **kwargs):
        super(ValidateUserView, self).__init__(**kwargs)
        self.username = None
        self.user = None

    @transaction.atomic()
    def action(self, serializer):
        self.username = serializer.data["username"]
        try:
            self.user = User.objects.get(username=self.username)
        except User.DoesNotExist:
            self.user = User.objects.create_user(username=self.username)
        self.user.token = utils.get_or_create_token(self.user, serializer.data["client"]).key
        data = serializers.RegistrationSerializer(self.user, context={'request': self.request}).data
        return response.Response(data=data)


class LogoutView(generics.GenericAPIView):
    """
    use this endpoint to logout an user.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    @transaction.atomic()
    def post(self, request):
        if request.auth:
            request.auth.delete()
            django.contrib.auth.signals.user_logged_out.send(sender=request.user.__class__, request=request,
                                                             user=request.user)
        return response.Response(status=status.HTTP_200_OK)


class UpdateProfileView(generics.UpdateAPIView):
    """
    use this endpoint to update profile view.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.UpdateProfileSerializer

    def get_object(self):
        return self.request.user
