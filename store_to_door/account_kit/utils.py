import binascii
import os

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import exceptions

from store_to_door import constants
from users import models


def int_to_role(pk):
    try:
        return models.Role.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_ROLE]})


class ActionViewMixin(object):

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return self.action(serializer)
        else:
            raise exceptions.ValidationError(serializer.errors)

    def put(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return self.action(serializer)
        else:
            raise exceptions.ValidationError(serializer.errors)


class GenerateTokenMixin(object):

    def get_or_create_token(self, serializer_data=None):
        serializer_data = serializer_data or {}
        token_data = dict((k, v) for k, v in serializer_data.items() if k in models.Token.LOGIN_FIELDS)
        token, _ = models.Token.objects.get_or_create(user=self.user, **token_data)
        return token


def get_or_create_token(user, client):
    token, created = models.Token.objects.get_or_create(client=client, user=user,
                                                        defaults={'user': user, 'client': client,
                                                                  'key': binascii.hexlify(os.urandom(20)).decode()})
    return token
