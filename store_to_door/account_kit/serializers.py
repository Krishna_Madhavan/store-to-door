from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import serializers, exceptions

from account_kit import utils
from store_to_door import constants
from users import models


class RegistrationSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    token = serializers.CharField(read_only=True, required=False)

    class Meta:
        model = User
        fields = ("id", "first_name", "email", "username", "token")

    @transaction.atomic()
    def create(self, validated_data):
        role = utils.int_to_role(2)
        validated_data["is_active"] = False
        user = User.objects.create_user(**validated_data)
        models.UserProfile.objects.create(user=user, role=role)
        return user

    @staticmethod
    def validate_email(attrs):
        if User.objects.filter(email=attrs).exists():
            raise exceptions.ValidationError({"non_field_errors": [constants.EMAIL_ALREADY_EXISTS]})
        return attrs

    @staticmethod
    def validate_username(attrs):
        if User.objects.filter(username=attrs).exists():
            raise exceptions.ValidationError({"non_field_errors": [constants.NUMBER_ALREADY_EXISTS]})
        return attrs


class ActivationSerializer(serializers.Serializer):
    username = serializers.CharField()
    authorization_code = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class ValidateUserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=10)
    client = serializers.CharField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class UpdateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "email", "first_name")
