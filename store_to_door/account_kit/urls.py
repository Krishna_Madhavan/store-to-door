from django.conf.urls import url

from account_kit import views

urlpatterns = [
    url(r'^register/$', views.RegistrationView.as_view(), name='registration'),
    url(r'^activate-account/$', views.ActivationView.as_view(), name='activate_account'),
    url(r'^validate-user/$', views.ValidateUserView.as_view(), name='validate_user'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^update/profile/$', views.UpdateProfileView.as_view(), name='update_profile'),
]
