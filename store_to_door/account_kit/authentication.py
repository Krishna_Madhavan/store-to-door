from rest_framework import authentication

from users import models


class TokenAuthentication(authentication.TokenAuthentication):
    model = models.Token
