from django.core.exceptions import ObjectDoesNotExist
from rest_framework import exceptions

from store_to_door import constants
from users import models


def int_to_product(pk):
    try:
        return models.Product.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_PRODUCT]})


def int_to_delivery_address(pk):
    try:
        return models.DeliveryAddress.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_ADDRESS]})


def int_to_store(pk):
    try:
        return models.Store.objects.get(pk=pk)
    except ObjectDoesNotExist:
        raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_STORE]})
