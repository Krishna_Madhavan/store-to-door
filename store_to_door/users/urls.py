from django.conf.urls import url

from users import views

urlpatterns = [
    url(r'^home-page/$', views.HomePageView.as_view(), name='home_page'),
    url(r'retrieve/product/(?P<pk>[0-9]+)/$', views.RetrieveProductView.as_view(), name='retrieve_product'),
    url(r'^list/products/(?P<pk>[0-9]+)/$', views.ListProductsView.as_view(), name='list_products'),
    url(r'^list/stores/$', views.ListStoresView.as_view(), name='list_stores'),
    url(r'^list/store-products/(?P<pk>[0-9]+)/$', views.ListStoreProductsView.as_view(), name='list_store_products'),
    url(r'^create/destroy/wishlist/(?P<pk>[0-9]+)/$', views.CreateDestroyWishlistView.as_view(),
        name='create_destroy_wishlist'),
    url(r'^list/my-wishlist/$', views.ListWishListView.as_view(), name='list_my_wishlist'),
    url(r'^list/create/my-cart/$', views.ListCreateCartView.as_view(), name='list_create_my_cart'),
    url(r'^update/cart/$', views.UpdateCartView.as_view(), name='update_cart'),
    url(r'^destroy/my-cart/(?P<pk>[0-9]+)/$', views.DestroyCartView.as_view(), name='destroy_my_cart'),
    url(r'^list/create/delivery-address/$', views.ListCreateDeliveryAddressView.as_view(),
        name='list_create_delivery_address'),
    url(r'^my-cart/checkout/$', views.CheckOutCartView.as_view(), name='my_cart_checkout'),
    url(r'^payment-success/$', views.PaymentSuccessView.as_view(), name='payment_success'),
    url(r'^my-orders/$', views.MyOrdersView.as_view(), name='my_orders'),
    url(r'^list/promotions/$', views.ListPromotionsView.as_view(), name='list_promotions'),
]
