from annoying.functions import get_object_or_None
from rest_framework import serializers, exceptions

from store_to_door import constants
from users import models


class ProductSerializer(serializers.ModelSerializer):
    in_cart = serializers.BooleanField(required=False, read_only=True)
    in_wishlist = serializers.BooleanField(required=False, read_only=True)

    class Meta:
        model = models.Product
        fields = ("id", "name", "unit", "apparent_price", "actual_price", "image", "discount", "in_cart", "in_wishlist")


class DailyOfferSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = models.DailyOffer
        fields = ("id", "product", "discount")


class PromotionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Promotion
        fields = ("id", "sub_category", "image")


class ProductSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductSubCategory
        fields = ("id", "name", "image")


class ProductCategorySerializer(serializers.ModelSerializer):
    sub_category = ProductSubCategorySerializer(many=True)

    class Meta:
        model = models.ProductCategory
        fields = ("id", "name", "image", "description", "sub_category")


class RetrieveProductSerializer(serializers.ModelSerializer):
    in_wishlist = serializers.BooleanField()
    in_cart = serializers.BooleanField()

    class Meta:
        model = models.Product
        fields = ("id", "name", "unit", "apparent_price", "actual_price", "image", "discount", "description",
                  "contents", "in_wishlist", "in_cart")


class ListProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProductCategory
        fields = ("id", "name", "image", "description")


class StoreCategorySerializer(serializers.ModelSerializer):
    category = ListProductCategorySerializer()

    class Meta:
        model = models.StoreCategory
        fields = ("id", "category")


class StoreSubCategorySerializer(serializers.ModelSerializer):
    sub_category = ProductSubCategorySerializer()

    class Meta:
        model = models.StoreSubCategory
        fields = ("id", "sub_category")


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Store
        fields = ("id", "name", "delivery_time", "free_deliver_cost", "rating")


class WishListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Wishlist
        fields = ("id", "user", "product")


class ListWishListSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = models.Wishlist
        fields = ("id", "user", "product")


class CreateCartSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cart
        fields = ("id", "product", "quantity")

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        queryset = get_object_or_None(models.Cart, user=validated_data["user"], product=validated_data["product"])
        if queryset:
            raise exceptions.ValidationError({"non_field_errors": [constants.ITEM_ALREADY_IN_CART]})
        return models.Cart.objects.create(**validated_data)


class ListCartSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = models.Cart
        fields = ("id", "product", "quantity")


class UpdateCartSerializer(serializers.Serializer):
    cart_items = serializers.ListField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class DeliveryAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DeliveryAddress
        fields = ("id", "street", "area", "city", "state", "country", "zipcode")

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        return models.DeliveryAddress.objects.create(**validated_data)


class ManualPaymentSerializer(serializers.Serializer):
    razorpay_order_id = serializers.CharField()
    razorpay_payment_id = serializers.CharField()
    razorpay_signature = serializers.CharField()
    store = serializers.IntegerField()
    address = serializers.IntegerField()

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class OrderedItemsSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = models.OrderedItems
        fields = "__all__"


class MyOrdersSerializer(serializers.ModelSerializer):
    store = StoreSerializer()
    address = DeliveryAddressSerializer()
    ordered_items = OrderedItemsSerializer(many=True)

    class Meta:
        model = models.Order
        fields = "__all__"
