import binascii
import os

from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


# Create your models here.
class Role(models.Model):
    objects = None
    name = models.CharField(max_length=254, unique=True)  # ((1, "Super Admin"), (2, "User"))

    class Meta:
        db_table = "role"


class UserProfile(models.Model):
    objects = None
    user = models.OneToOneField(User)
    role = models.ForeignKey(Role)
    profile_pic = models.ImageField(upload_to="profile_pic")

    class Meta:
        db_table = "user-profile"


class Token(models.Model):
    objects = None
    key = models.CharField(max_length=40, primary_key=True)
    user = models.ForeignKey(User, related_name='auth_tokens')
    client = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'multitoken'
        unique_together = ('user', 'client',)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key
        return super(Token, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return u'{0} (client: {1})'.format(self.key, self.client)

    LOGIN_FIELDS = (
        'client',
    )


class ProductCategory(models.Model):
    objects = None
    name = models.CharField(max_length=254, unique=True)
    image = models.URLField()
    description = models.TextField()

    class Meta:
        db_table = "product_category"


class ProductSubCategory(models.Model):
    objects = None
    category = models.ForeignKey(ProductCategory, related_name="sub_category")
    name = models.CharField(max_length=254)
    image = models.URLField()

    class Meta:
        db_table = "product_subcategory"
        unique_together = ("category", "name")


class Product(models.Model):
    objects = None
    sub_category = models.ForeignKey(ProductSubCategory, related_name='products')
    name = models.CharField(max_length=254)
    unit = models.CharField(max_length=254)
    apparent_price = models.FloatField()
    actual_price = models.FloatField()
    discount = models.CharField(max_length=254)
    description = models.TextField()
    contents = models.TextField()
    image = models.URLField(blank=True, null=True)

    class Meta:
        db_table = "product"
        unique_together = ("sub_category", "name")


class Store(models.Model):
    objects = None
    name = models.CharField(max_length=254, unique=True)
    delivery_time = models.CharField(max_length=254)
    free_deliver_cost = models.FloatField()
    rating = models.FloatField()

    class Meta:
        db_table = "store"


class StoreCategory(models.Model):
    objects = None
    store = models.ForeignKey(Store, related_name="store_category")
    category = models.ForeignKey(ProductCategory)

    class Meta:
        db_table = "store_category"
        unique_together = ("store", "category")


class StoreSubCategory(models.Model):
    objects = None
    store_category = models.ForeignKey(StoreCategory, related_name="store_sub_category")
    sub_category = models.ForeignKey(ProductSubCategory)

    class Meta:
        db_table = "store_subcategory"


class StoreProduct(models.Model):
    objects = None
    store_subcategory = models.ForeignKey(StoreSubCategory)
    product = models.ForeignKey(Product)

    class Meta:
        db_table = "store_product"
        unique_together = ("store_subcategory", "product")


class DailyOffer(models.Model):
    objects = None
    product = models.OneToOneField(Product)
    discount = models.FloatField()

    class Meta:
        db_table = "daily_offers"


class Promotion(models.Model):
    objects = None
    sub_category = models.ForeignKey(ProductSubCategory)
    image = models.ImageField()

    class Meta:
        db_table = "promotion"


class Wishlist(models.Model):
    objects = None
    user = models.ForeignKey(User, related_name="wishlist")
    product = models.ForeignKey(Product)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "wishlist"
        unique_together = ("user", "product")


class Cart(models.Model):
    objects = None
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    quantity = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(9)])
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "cart"
        unique_together = ("user", "product")


class DeliveryAddress(models.Model):
    objects = None
    user = models.ForeignKey(User)
    street = models.CharField(max_length=254)
    area = models.CharField(max_length=254)
    city = models.CharField(max_length=254)
    state = models.CharField(max_length=254)
    country = models.CharField(max_length=254)
    zipcode = models.IntegerField()

    class Meta:
        db_table = "delivery_address"


class Order(models.Model):
    objects = None
    user = models.ForeignKey(User)
    store = models.ForeignKey(Store)
    total_cost = models.FloatField(default=0.0)
    address = models.ForeignKey(DeliveryAddress)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "order"


class OrderedItems(models.Model):
    objects = None
    order = models.ForeignKey(Order, related_name="ordered_items")
    product = models.ForeignKey(Product)
    quantity = models.IntegerField()

    class Meta:
        db_table = "ordered_items"


class Payment(models.Model):
    objects = None
    order = models.OneToOneField(Order)
    txn_id = models.CharField(max_length=254)
    total_cost = models.FloatField()
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "payment"
