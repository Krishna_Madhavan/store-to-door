import hashlib
import hmac

import razorpay
from annoying.functions import get_object_or_None
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from rest_framework import generics, permissions, response, status, exceptions

from account_kit.utils import ActionViewMixin
from store_to_door import constants
from users import models, serializers, utils


class HomePageView(generics.GenericAPIView):
    """
    use this endpoint to list data for landing page.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def __init__(self, **kwargs):
        super(HomePageView, self).__init__(**kwargs)
        self.daily_offers = serializers.DailyOfferSerializer
        self.promotion = serializers.PromotionSerializer
        self.product_category = serializers.ProductCategorySerializer

    def get(self, request):
        data = dict()
        daily_offers = models.DailyOffer.objects.all()
        for x in daily_offers:
            x.product.in_cart = True if get_object_or_None(models.Cart, user=request.user, product=x.product) else False
            x.product.in_wishlist = True if get_object_or_None(models.Wishlist, user=request.user, product=x.product
                                                               ) else False
        data["daily_offers"] = self.daily_offers(daily_offers, many=True).data
        promotion = models.Promotion.objects.all()
        data["promotion"] = self.promotion(promotion, many=True, context={"request": request}).data
        product_category = models.ProductCategory.objects.all()
        data["product_category"] = self.product_category(product_category, many=True).data
        return response.Response(data, status=status.HTTP_200_OK)


class RetrieveProductView(generics.RetrieveAPIView):
    """
    use this endpoint to retrieve a product.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.RetrieveProductSerializer

    def get_object(self):
        product = utils.int_to_product(self.kwargs["pk"])
        product.in_wishlist = True if get_object_or_None(models.Wishlist, user=self.request.user, product=product
                                                         ) else False
        product.in_cart = True if get_object_or_None(models.Cart, user=self.request.user, product=product) else False
        return product


class ListProductsView(generics.ListAPIView):
    """
    use this endpoint to list all products given a sub-category id.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.ProductSerializer

    def get_queryset(self):
        queryset = models.Product.objects.filter(sub_category=self.kwargs["pk"])
        for product in queryset:
            product.in_cart = True if get_object_or_None(models.Cart, user=self.request.user, product=product
                                                         ) else False
            product.in_wishlist = True if get_object_or_None(models.Wishlist, user=self.request.user, product=product
                                                             ) else False
        return queryset


class ListStoresView(generics.ListAPIView):
    """
    use this endpoint to list stores.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.StoreSerializer
    queryset = models.Store.objects.all()


class ListStoreProductsView(generics.ListAPIView):
    """
    use this endpoint to list the products of a store given the store's sub-category id.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.ProductSerializer

    def get_queryset(self):
        store_products = models.StoreProduct.objects.filter(store_subcategory=self.kwargs["pk"])
        return [store_product.product for store_product in store_products]


class CreateDestroyWishlistView(generics.GenericAPIView):
    """
    use this endpoint to either add a product to user's wishlist or to delete a product from user's wishlist.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    @transaction.atomic()
    def post(self, request, pk):
        user = request.user

        try:
            wishlist = models.Wishlist.objects.get(user=user, product=pk)
        except ObjectDoesNotExist:
            wishlist = None

        if wishlist:
            wishlist.delete()
            return response.Response({"in_wishlist": False})
        serializer = serializers.WishListSerializer(data={"user": user.id, "product": pk})
        if serializer.is_valid():
            serializer.save()
            return response.Response({"in_wishlist": True})
        else:
            raise exceptions.ValidationError(serializer.errors)


class ListWishListView(generics.ListAPIView):
    """
    use this endpoint to list my-wishlist.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.ListWishListSerializer

    def get_queryset(self):
        return models.Wishlist.objects.filter(user=self.request.user)


class ListCreateCartView(generics.ListCreateAPIView):
    """
    use this endpoint to add an item to cart or to list an users cart.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get_serializer_class(self):
        if self.request.method == "POST":
            return serializers.CreateCartSerializer
        else:
            return serializers.ListCartSerializer

    def get_queryset(self):
        return models.Cart.objects.filter(user=self.request.user)


class UpdateCartView(ActionViewMixin, generics.GenericAPIView):
    """
    use this endpoint to update quantity of items in the cart.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.UpdateCartSerializer

    @transaction.atomic()
    def action(self, serializer):
        for item in serializer.data['cart_items']:
            cart = self.get_cart_item(item['pk'])
            if int(item['quantity']) > 9 or int(item['quantity']) < 1:
                raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_QUANTITY_ERROR]})
            cart.quantity = item['quantity']
            cart.save()
        return response.Response({"data": "Cart updated."})

    def get_cart_item(self, pk):
        try:
            return models.Cart.objects.get(pk=pk, user=self.request.user)
        except ObjectDoesNotExist:
            raise exceptions.ValidationError({"non_field_errors": [constants.INVALID_PRODUCT]})


class DestroyCartView(generics.DestroyAPIView):
    """
    use this endpoint to delete a product from cart.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    queryset = models.Cart.objects.all()


class ListCreateDeliveryAddressView(generics.ListCreateAPIView):
    """
    use this endpoint to list all delivery address for an user or to create a new one.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.DeliveryAddressSerializer

    def get_queryset(self):
        return models.DeliveryAddress.objects.filter(user=self.request.user)


class CheckOutCartView(generics.GenericAPIView):
    """
    use this endpoint to checkout cart items.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def get(self, request):
        user = request.user
        client = razorpay.Client(auth=(constants.YOUR_API_KEY, constants.YOUR_API_SECRET))
        cost_in_rupees, cost_in_paise = self.get_amount(user)
        resp = client.order.create(data={'currency': 'INR', 'receipt': '123456', 'payment_capture': True,
                                         'amount': cost_in_paise})
        return response.Response({'order_id': resp['id'], 'amount': resp['amount']})

    @staticmethod
    def get_amount(user):
        total_cost = 0.0
        cart_items = models.Cart.objects.filter(user=user)
        if not cart_items:
            raise exceptions.ValidationError({"non_field_errors": [constants.EMPTY_CART]})
        for item in cart_items:
            total_cost += float(item.product.actual_price) * int(item.quantity)

        return total_cost, total_cost * 100


class PaymentSuccessView(ActionViewMixin, generics.GenericAPIView):
    """
    use this endpoint to return payment success manually.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.ManualPaymentSerializer

    @transaction.atomic()
    def action(self, serializer):
        user = self.request.user
        string = str(serializer.data['razorpay_order_id']) + "|" + str(serializer.data['razorpay_payment_id'])
        dig = hmac.new(constants.YOUR_API_SECRET.encode('utf-8'), msg=string.encode('utf-8'),
                       digestmod=hashlib.sha256).hexdigest()

        if str(dig) == str(serializer.data['razorpay_signature']):
            total_cost, _ = self.get_amount(user)
            store = utils.int_to_store(serializer.data["store"])
            address = utils.int_to_delivery_address(serializer.data["address"])
            order = models.Order.objects.create(user=user, store=store, address=address,
                                                total_cost=total_cost)

            cart_items = models.Cart.objects.filter(user=user)
            if not cart_items:
                raise exceptions.ValidationError({"non_field_errors": ["The cart is empty."]})
            for item in cart_items:
                models.OrderedItems.objects.create(order=order, product=item.product, quantity=item.quantity)

            for item in cart_items:
                item.delete()

            models.Payment.objects.create(order=order, total_cost=total_cost,
                                          txn_id=serializer.data['razorpay_order_id'])
            return response.Response({'data': 'success'})
        else:
            raise exceptions.ValidationError({"non_field_errors": ["Signature mismatch."]})

    @staticmethod
    def get_amount(user):
        total_cost = 0.0
        cart_items = models.Cart.objects.filter(user=user)
        if not cart_items:
            raise exceptions.ValidationError({"non_field_errors": [constants.EMPTY_CART]})
        for item in cart_items:
            total_cost += float(item.product.actual_price) * int(item.quantity)

        return total_cost, total_cost * 100


class MyOrdersView(generics.ListAPIView):
    """
    use this endpoint to list my orders.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.MyOrdersSerializer

    def get_queryset(self):
        return models.Order.objects.filter(user=self.request.user)


class ListPromotionsView(generics.ListAPIView):
    """
    use this endpoint to list promotions.
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = serializers.PromotionSerializer
    queryset = models.Promotion.objects.all()
